MfGames.IO CIL
==============

This a small collection of classes and extensions to make working with System.IO a little bit easier.

## Installation

At the moment, this library is not on [NuGet.org](https://nuget.org/). Instead,
it is hosted at [MyGet](https://www.myget.org/feed/Packages/mfgames).

```
dotnet add package MfGames.IO --source https://www.myget.org/F/mfgames/api/v3/index.json 
```

The repository can also be added to a project by setting the `NuGet.Config` file.

```
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <add key="nuget.org" value="https://api.nuget.org/v3/index.json" protocolVersion="3" />
    <add key="mfgames" value="https://www.myget.org/F/mfgames/api/v3/index.json" protocolVersion="3" />
  </packageSources>
</configuration>
```

## Extensions

### Assembly Extensions

#### `DirectoryInfo? GetDirectory(this Assembly? assembly)`

Gets a directory containing the assembly directory.

#### `FileInfo? GetFile(this Assembly? assembly)`

Gets a file representing the assembly's `Location` property or null.

### DirectoryInfo Extensions

#### `DirectoryInfo? CreateIfMissing(this DirectoryInfo? directory)`

Creates `directory` if it doesn't exist and `directory` is not null. This will
also create any parent directories to duplicate the `mkdir -p directory`
functionality that inspired it.

#### `DirectoryInfo? FindGitRoot(this DirectoryInfo?)`

Finds the Git root from the given directory, using `FindSelfOrParent`.

#### `DirectoryInfo? FindParent(this DirectoryInfo?, Func<DirectoryInfo, bool> match)`

Finds a parent directory that returns true from the given `match`. This does not
check the given `directory`, but only parents. If none are found, this returns
null.

#### `DirectoryInfo? FindSelfOrParent(this DirectoryInfo?, Func<DirectoryInfo, bool> match)`

Finds a parent directory that returns true from the given `match`. This checks
the given `directory` before going to parents. If none are found, this returns
null.

#### `DirectoryInfo GetDirectory(this DirectoryInfo directory, string[] paths)`

The equivalent of `new DirectoryInfo(Path.Combine(directory.FullName, ...paths))`.

#### `FileInfo GetFile(this DirectoryInfo directory, string[] paths)`

The equivalent of `new FileInfo(Path.Combine(directory.FullName, ...paths))`.

#### `bool IsGitRoot(this DirectoryInfo? directory)`

Returns `true` if the given directory is non-null and contains the `.git` folder.
Otherwise, returns false.

### FileInfo Extensions

#### `string ReadAllText(this FileInfo file)`

The same as `File.ReadAllText(file.GetFullPath)`.

#### `string ReadAllText(this FileInfo file, Encoding encoding)`

The same as `File.ReadAllText(file.GetFullPath, encoding)`.

#### `void WriteAllText(this FileInfo file, string text)`

The same as `File.WriteAllText(file.GetFullPath, text)`.

#### `void WriteAllText(this FileInfo file, string text, Encoding encoding)`

The same as `File.WriteAllText(file.GetFullPath, text, encoding)`.

### Type Extensions

#### `DirectoryInfo? GetDirectory(this Type? type)`

Gets a directory containing the type's assembly directory.

#### `FileInfo? GetFile(this Type? type)`

Gets a file representing the type's assembly's `Location` property or null.
