using System.IO;

using MfGames.IO.Extensions;

using Xunit;

namespace MfGames.IO.Tests
{
    public class GitRootTests
    {
        [Fact]
        public void FindGitRoot()
        {
            DirectoryInfo? dir = this.GetType().GetDirectory();
            DirectoryInfo? git = dir.FindGitRoot();

            Assert.NotNull(git);
            Assert.True(git!.GetDirectories(".git").Length > 0);
        }
    }
}
