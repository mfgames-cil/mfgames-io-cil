using System.IO;

using MfGames.IO.Extensions;

using Xunit;

namespace MfGames.IO.Tests
{
    public class AssemblyExtensionsTests
    {
        [Fact]
        public void DirectoryExists()
        {
            DirectoryInfo? dir = this.GetType().Assembly.GetDirectory();

            Assert.NotNull(dir);
            Assert.True(dir!.Exists);
        }

        [Fact]
        public void FileExists()
        {
            FileInfo? file = this.GetType().Assembly.GetFile();

            Assert.NotNull(file);
            Assert.True(file!.Exists);
        }
    }
}
