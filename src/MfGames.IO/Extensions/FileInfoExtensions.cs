using System.IO;
using System.Text;

namespace MfGames.IO.Extensions
{
    /// <summary>
    /// Useful extension methods for FileInfo.
    /// </summary>
    public static class FileInfoExtensions
    {
        /// <summary>
        /// Reads all the text from a FileInfo object.
        /// </summary>
        /// <param name="file">The file to read from.</param>
        /// <returns>The text contents.</returns>
        public static string ReadAllText(this FileInfo file)
        {
            return File.ReadAllText(file.FullName);
        }

        /// <summary>
        /// Reads all the text from a FileInfo object.
        /// </summary>
        /// <param name="file">The file to read from.</param>
        /// <param name="encoding">The encoding to use.</param>
        /// <returns>The text contents.</returns>
        public static string ReadAllText(this FileInfo file, Encoding encoding)
        {
            return File.ReadAllText(file.FullName, encoding);
        }

        /// <summary>
        /// Writes out all the text to the given file.
        /// </summary>
        /// <param name="file">The file to write to.</param>
        /// <param name="text">The text to write out.</param>
        public static void WriteAllText(this FileInfo file, string text)
        {
            File.WriteAllText(file.FullName, text);
        }

        /// <summary>
        /// Writes out all the text to the given file.
        /// </summary>
        /// <param name="file">The file to write to.</param>
        /// <param name="text">The text to write out.</param>
        /// <param name="encoding">The encoding to use.</param>
        public static void WriteAllText(
            this FileInfo file,
            string text,
            Encoding encoding)
        {
            File.WriteAllText(file.FullName, text, encoding);
        }
    }
}
