using System;
using System.IO;

namespace MfGames.IO.Extensions
{
    /// <summary>
    /// Additional methods for types.
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Gets the directory for a given type.
        /// </summary>
        /// <param name="type">The type assembly to get the directory.</param>
        /// <returns>The directory or null if the assembly or location is null.</returns>
        public static DirectoryInfo? GetDirectory(this Type? type)
        {
            return type?.Assembly.GetDirectory();
        }

        /// <summary>
        /// Gets the file for a given type's assembly.
        /// </summary>
        /// <param name="type">The type assembly to get the directory.</param>
        /// <returns>The directory or null if the assembly or location is null.</returns>
        public static FileInfo? GetFile(this Type? type)
        {
            return type?.Assembly.GetFile();
        }
    }
}
