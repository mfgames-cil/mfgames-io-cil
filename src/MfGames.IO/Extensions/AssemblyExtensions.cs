using System.IO;
using System.Reflection;

namespace MfGames.IO.Extensions
{
    /// <summary>
    /// Additional methods for assemblies.
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Gets the directory for a given assembly.
        /// </summary>
        /// <param name="assembly">The assembly to get the directory.</param>
        /// <returns>The directory or null if the assembly or location is null.</returns>
        public static DirectoryInfo? GetDirectory(this Assembly? assembly)
        {
            return assembly.GetFile()?.Directory;
        }

        /// <summary>
        /// Gets the file for a given assembly.
        /// </summary>
        /// <param name="assembly">The assembly to get the directory.</param>
        /// <returns>The directory or null if the assembly or location is null.</returns>
        public static FileInfo? GetFile(this Assembly? assembly)
        {
            return assembly?.Location == null
                ? null
                : new FileInfo(assembly.Location);
        }
    }
}
