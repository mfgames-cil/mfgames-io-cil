## [1.2.3](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.2.2...v1.2.3) (2021-12-07)


### Bug Fixes

* fixed the Git root searching plus added tests ([37f5e3f](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/37f5e3f4cbb8a6ed1afecdc6c0514b8fd0d914ea))

## [1.2.2](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.2.1...v1.2.2) (2021-12-07)


### Bug Fixes

* assemblies are files, not directories ([e69971d](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/e69971df7fa455b91bcd3f1a66939373246822ca))

## [1.2.1](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.2.0...v1.2.1) (2021-12-07)


### Bug Fixes

* handle directory.Exists caching ([233656a](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/233656abb9417819ea7044067dd194831ae10340))

# [1.2.0](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.1.0...v1.2.0) (2021-12-07)


### Bug Fixes

* protect against non-existent directories while finding ([2b562e8](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/2b562e8c34a9de41cf6aceaf4d162ef978e6b6ef))


### Features

* added assembly.GetDirectory() ([53aacbf](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/53aacbf862696f0057371883d936575928896323))

# [1.1.0](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.5...v1.1.0) (2021-12-07)


### Features

* added more extension methods, updated documentation ([cca890b](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/cca890b061743f1c9192b40aa81beaddbcdb2e6f))

## [1.0.5](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.4...v1.0.5) (2021-09-11)


### Bug Fixes

* **nuget:** fixing packaging and versioning ([29fd621](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/29fd621de05ec5390d70d7858396a99012c3748d))
* **nuget:** fixing packaging and versioning ([24faa43](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/24faa43abe18e4cd28ff4faf41110772648b61b6))
* **nuget:** fixing packaging and versioning ([4eca768](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/4eca7682a3c3165a4f25cffcae64761d04a7b67d))

## [1.0.4](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.3...v1.0.4) (2021-09-11)


### Bug Fixes

* moving directory props in the same place ([7d55de8](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/7d55de864cccd88d53451b1c6adb5252a0ca3b72))

## [1.0.3](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.2...v1.0.3) (2021-09-11)


### Bug Fixes

* **semantic-release-nuget:** correcting package version race condition ([f3966a7](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/f3966a73781ee1d92a179ca80760515dbbc4bc86))

## [1.0.2](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.1...v1.0.2) (2021-09-11)


### Bug Fixes

* updating readme to trigger build ([dade5a7](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/dade5a7f9030027af23b2935a8e17bbe6d99cdcb))

## [1.0.1](https://gitlab.com/mfgames-cil/mfgames-io-cil/compare/v1.0.0...v1.0.1) (2021-09-11)


### Bug Fixes

* updating NuGet description ([2aa87a9](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/2aa87a9115a8baa916e3cc458fc0a8912967b988))

# 1.0.0 (2021-09-11)


### Features

* initial commit ([5adb9cc](https://gitlab.com/mfgames-cil/mfgames-io-cil/commit/5adb9ccc52fd75fbd6a7859e1ce221d0d3b9bae7))
